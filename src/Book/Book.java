package Book;

public class Book {
    public Book(String author, String titleOfBook, int yearOfPublication, Genre genre, String url) {
        this.author = author;
        this.book = titleOfBook;
        this.yearOfPublication = yearOfPublication;
        this.genre = genre;
        this.url = url;
    }

    private String author;
    private String book;
    private int yearOfPublication;
    private Genre genre;
    public String url;

    public String getAuthor() {
        return author;
    }

    public String getTitleOfBook() {
        return book;
    }

    public int getYearOfPublication() {
        return yearOfPublication;
    }

    public Genre getGenre() {
        return genre;
    }

    public String getUrl() {
        return url;
    }
}
